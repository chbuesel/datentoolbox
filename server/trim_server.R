getData_trim <- reactive({
  
  inFile <- input$df_trim
  
  if (is.null(input$df_trim))
    return(NULL)
  
  read.csv(inFile$datapath, header=input$header_trim, sep=input$sep_trim, 
           quote=input$quote_trim)
  
  
})

output$contents_trim <- renderTable({
  
  lala_trim <- getData_trim()
  
  if(input$disp_trim == "head") {
    return(head(lala_trim))
  }
  else {
    return(lala_trim)
  }
  
})

output$trimmed_trim <- renderTable({
  
  mod_trim <- trimming(df = getData_trim(), cutlower = input$colower_trim, cutupper = input$coupper_trim, method = input$proc_trim,
                       sds = as.numeric(input$sds_trim), measure = input$meanmedian_trim, var1 = input$var1_trim, var2 = input$var2_trim, 
                       var3 = input$var3_trim, var4 = input$var4_trim, var5 = input$var5_trim, var6 = input$var6_trim, 
                       var7 = input$var7_trim, dvrt = input$dv_trim)
  
  if(input$disp_trim == "head") {
    return(head(mod_trim))
  }
  else {
    return(mod_trim)
  }
  
})

output$dataloss <- renderText({
  data_loss(df1 = getData_trim(),
            df2 = trimming(df = getData_trim(), cutlower = input$colower_trim, cutupper = input$coupper_trim, method = input$proc_trim,
                           sds = as.numeric(input$sds_trim), measure = input$meanmedian_trim, var1 = input$var1_trim, var2 = input$var2_trim, 
                           var3 = input$var3_trim, var4 = input$var4_trim, var5 = input$var5_trim, var6 = input$var6_trim, 
                           var7 = input$var7_trim, dvrt = input$dv_trim))
})

output$downloadData_trim <- downloadHandler(
  
  
  filename = function() {
    paste("data-trimmed", Sys.Date(), ".csv", sep="")
  },
  content = function(file) {
    write.csv(trimming(df = getData_trim(), cutlower = input$colower_trim, cutupper = input$coupper_trim, method = input$proc_trim,
                       sds = as.numeric(input$sds_trim), measure = input$meanmedian_trim, var1 = input$var1_trim, var2 = input$var2_trim, 
                       var3 = input$var3_trim, var4 = input$var4_trim, var5 = input$var5_trim, var6 = input$var6_trim, var7 = input$var7_trim, 
                       dvrt = input$dv_trim), 
              file, row.names = FALSE)
  }
)